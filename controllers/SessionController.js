/*
  Index = listagem de sessoes,
  Store = Criar uma sessao,
  Show = Listagem de uma unica sessao,
  update = Alterar alguma sessao,
  destroy = deletar alguma sessao
*/

const User = require("../models/User");

class SessionControler {
  async store(req, res) {
    const { email } = req.body;

    let user = await User.findOne({ email });

    if (!user) {
      user = await User.create({
        email
      });
    }

    return res.json(user);
  }
}

module.exports = new SessionControler();
