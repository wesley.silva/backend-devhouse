const Reserva = require("../models/Reserva");
const User = require("../models/User");
const House = require("../models/House");

class ReservaController {
  async index(req, res) {
    const { user_id } = req.headers;

    const reservas = await Reserva.find({
      user: user_id
    }).populate("house");

    return res.json(reservas);
  }

  async store(req, res) {
    const { user_id } = req.headers;
    const { house_id } = req.params;
    const { data } = req.body;

    const house = await House.findById(house_id);
    if (!house) {
      return res.status(400).json({
        erro: "Casa não existe"
      });
    }

    if (house.status !== true) {
      return res.status(400).json({
        erro: "Solicitação indisponível"
      });
    }

    const user = await User.findById(user_id);
    if (String(user._id) === String(house.user)) {
      return res.status(401).json({
        erro: "Reserva não permitida"
      });
    }

    const reserve = await Reserva.create({
      user: user_id,
      house: house_id,
      date
    });

    await reserve
      .populate("house")
      .populate("user")
      .execPopulate();

    return json(reserve);
  }

  async destroy(req, res) {
    const { reserve_id } = req.body;

    await Reserva.findByIdAndDelete({
      _id: reserve_id
    });

    res.send();
  }
}

module.exports = new ReservaController();
