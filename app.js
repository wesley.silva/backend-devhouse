const express = require("express");
const routes = require("./routes");
const mongoose = require("mongoose");
const path = require("path");
const cors = require("cors");
class App {
  constructor() {
    this.server = express();
    mongoose.connect(
      "mongodb+srv://brunowbbs:brunowbbs@cluster0-pdzrk.mongodb.net/test?retryWrites=true&w=majority",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    );
    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.server.use(cors());
    this.server.use(express.json());
    this.server.use(
      "/files",
      express.static(path.resolve(__dirname, "..", "uploads"))
    );
  }

  routes() {
    this.server.use(routes);
  }
}

module.exports = new App().server;
